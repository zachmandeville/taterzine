const choo = require("choo")
const main = require("./javascript-code/views/main.js")
const devTools = require('choo-devtools')

// Stores
var recipes = require('./javascript-code/stores/recipes.js')
// start up choo
var app = choo()

// Get helpful console prompts.
app.use(devTools())

// Load our Stores
app.use(recipes)

// When someone goes to the homepage, choo sends them our main view.
app.route('/', main)

// Start it all up by mounting to the body of index.html
app.mount('body')
