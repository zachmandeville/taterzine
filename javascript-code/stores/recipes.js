var smarkt = require('smarkt')

module.exports = (state, emitter) => {
    var archive = new DatArchive(window.location)
    state.dat = archive.url
    state.recipe = ''
    state.chef = 'Angelica'
    state.message = 'Hi, I love you and you are really cool.'
    
    emitter.on('DOMContentLoaded', () => {
        loadRecipe()
        console.log(`hey, you located at ${state.dat}`)
    })

    emitter.on('header clicked', (e) => {
        text = e.target
        state.chef = 'HACKER001'
        state.message = 'YOU HAVE BEEN HACKED BY '
        emitter.emit('render')
    })
    function loadRecipe () {
        archive.readFile('recipes/tatertot-casserole.txt')
            .then(contents => {
                state.recipe = smarkt.parse(contents)
                emitter.emit('render')
            })
    }
}
