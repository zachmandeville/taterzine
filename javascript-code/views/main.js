const html = require('nanohtml')
const header = require('./header.js')
const ingredients = require('./ingredients.js')
const directions = require('./directions.js')

module.exports = (state,emit) => {
    return html`
    <body>
${header(state,emit)}
${ingredients(state,emit)}
${directions(state,emit)}
    </body>
    `
}


