const html = require('nanohtml')

module.exports = (state, emit) => {
    if (state.recipe.recipe) {
    return html`
<div id='header'>
      <h1 onclick=${sendMessage}>${state.recipe.recipe}, by ${state.chef} </h1>
</div>
    `
    }

    function sendMessage (e) {
        console.log({theEvent: e})
        emit('header clicked', e)
    }
}

