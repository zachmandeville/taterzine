const html = require('nanohtml')
const md = require('markdown-it')()
const raw = require('nanohtml/raw')

module.exports = (state, emit) => {
    if (state.recipe.ingredients) {
    var ingredients = state.recipe.ingredients
    return html`
    <div>
        <h1>Ingredients!</h1>
    <ul>
        ${listIngredients(ingredients)}
    </ul>
    </div>
    `
    }

    function listIngredients (ingredients) {
        return ingredients.map(ingredient => {
        var encodedHTML = md.render(ingredient)
        var usableContent = raw(encodedHTML)
        return html`
          <li>${usableContent}</li>
        `
        })
    }
}
