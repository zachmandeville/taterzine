const html = require('nanohtml')

module.exports = (state, emit) => {
    if (state.recipe.directions) {
    var directions = state.recipe.directions
    return html`
<div>
      <h1>Directions!</h1>
<ul>
      ${listDirections(directions)}
</ul>
</div>
    `
    }

    function listDirections (directions) {
        return directions.map(direction => {
        return html`
          <li>${direction}</li>
        `
        })
    }
}
